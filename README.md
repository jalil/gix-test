# Testing gitoxide issue

this repository is there to test an issue in
[gitoxide](https://github.com/byron/gitoxide), specifically,
[issue 1061](https://github.com/byron/gitoxide/issues/1061).

## Reproducing

1. Create a git repo in a gitea instance
2. Clone it with an `ssh` url (or add it as a remote)
3. Clone it somewhere else the same way.
4. Add a few commits in one place.
5. Push the commits.
6. Try to fetch the new commits with `gix -c protocol.version=1 fetch`

Add at least 18 commits
